[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=saSlim_telerik_devops_upskill)](https://sonarcloud.io/summary/new_code?id=saSlim_telerik_devops_upskill)

### Overview ###

![Image](workflow.drawio.svg)




The repo demontsrates complete automation (almost) from creating an issue in Jira to seeing the app alive. Main goal is to achieve fast feed back loop and in order to do it we have connected Jira, Bitbucket and Slack. Jira and Slack are getting updates on each step

#### What have been used here ####
* Kubernetes on Azure AKS

* Terraform for IaC - this part of the implementation can be found [here](https://bitbucket.org/saSlim/telerik_devops_upskill_iac/src/master/)

* Jira
* Bitbucket Pipelines
* Slack 
* Docker
* Go lang for simple webservice running on nginx container

#### Workflow ####
* Team gets notified in Slack on each of the steps below

* Issue is created in Jira
* Developer starts work on an issue:
     - the issue gets transitioned from `To Do` to `In Progres` automatically (Jira Automation Triggers)
* Developer pushes working branch to remote:
     - Simple pipeline starts with only 2 steps: 
        - Lint Code ([golangci-lint](https://github.com/golangci/golangci-lint))
        - Secrets Scan ([git-secrets](https://bitbucket.org/atlassian/git-secrets-scan/src/master/))

* On Pull Request 
    - Jira issue is transitioned automatically to `In Review`
    - Reviewers can start Review directly from Slack
    - Another pipline is triggered:
        - Build 
        - Test (run unit tests)
        - Lint Code ([golangci-lint](https://github.com/golangci/golangci-lint))
        - Secrets Scan ([git-secrets](https://bitbucket.org/atlassian/git-secrets-scan/src/master/))

* Pull Request approved and merged `(Merge is not available if the pipeline triggered on pull request didn't succeed)`:

    - Jira issue is transitioned automatically to `Done`
    - Deployment pipeline gets involved:
        - Build 
        - Test (run unit tests)
        - Lint Code ([golangci-lint](https://github.com/golangci/golangci-lint))
        - Secrets Scan ([git-secrets](https://bitbucket.org/atlassian/git-secrets-scan/src/master/))
        - SAST ([Sonarcloud](https://sonarcloud.io))
        - Build Docker image
        - Lint Dockerfile ([hadolint](https://github.com/hadolint/hadolint))
        - Scan Docker image ([Snyk](https://snyk.io))
        - Upload to Docker Hub
        - Deploy to Azure AKS



### To Do ###

* Complete [this ToDo list](https://bitbucket.org/saSlim/telerik_devops_upskill_iac/src/master/README.md) which at the moment of writing looks like this:
    - &#x2611; Set up Azure storage to store Terraform state
    - Make it work via pipeline with the following steps: 
        - Check for hardcoded credentials
        - Check terraform code format
        - Check for infrastructure vulnerabilities with checkov
        - Send terraform plan to chat
        - Approve with ChatBot
        - Deploy the infrastructure

* Provide more info when step fails in Slack
* Compile Go app in one container (one step) and run it from another (i think i've messed GO paths somewhere)




