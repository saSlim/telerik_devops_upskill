FROM golang:1.12.7-alpine3.10 AS build
# Support CGO and SSL
RUN apk --no-cache add gcc g++ make git
WORKDIR /go/src/app
COPY . .
RUN go get github.com/gorilla/mux
RUN GOOS=linux go build -ldflags="-s -w" -o ./gowebservice ./webservice/main.go

FROM alpine:3.15
RUN apk --no-cache add ca-certificates
WORKDIR /usr/bin
COPY --from=build /go/src/app /go/bin
# COPY gowebservice /go/bin
EXPOSE 8080
ENTRYPOINT ["/go/bin/gowebservice", "--port", "8080"]